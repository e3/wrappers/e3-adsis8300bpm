###############################################################################
# Load E3 modules
###############################################################################
# Loading of all required modules and versions
require busy,1.7.2_c596e1c
require autosave,5.10.0
require asyn,4.37.0
require ADCore,3.9.0
require admisc,2.1.0
require iocStats,3.1.16
require recsync,1.3.0-9705e52
require mrfioc2,2.2.1rc1
require adsis8300bpm,devel
require iocmetadata,0.0.1

errlogInit(20000)
callbackSetQueueSize(15000)

#- load the instance definition
< instance.cmd

#- 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX",                       "$(CONTROL_GROUP):$(AMC_NAME):")
epicsEnvSet("PORT",                         "$(AMC_NAME)")
epicsEnvSet("MAX_RAW_SAMPLES",              "1600000")
epicsEnvSet("MAX_IQ_SAMPLES",               "110000")
#- AD plugin macros
#epicsEnvSet("XSIZE",                        "$(MAX_RAW_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")

#- sis8300bpmConfigure(const char *portName, const char *devicePath, int numSamples,
#-                     int maxBuffers, size_t maxMemory, int priority, int stackSize)
sis8300bpmConfigure("$(PORT)", "$(AMC_DEVICE)", $(MAX_RAW_SAMPLES), 0, 0)

#- BPM system
dbLoadRecords("sis8300bpm.template","P=$(PREFIX),R=,PORT=$(PORT),MAX_SAMPLES=$(MAX_RAW_SAMPLES)")
dbLoadRecords("sis8300-evr.template","P=$(PREFIX),R=,PORT=$(PORT),EVR_DEV=$(EVR_NAME)")
#- instance 1 parameters
dbLoadRecords("sis8300bpm-instance.template","P=$(SYSTEM1_PREFIX),R=,PORT=$(PORT),ADDR=0,NAME=$(SYSTEM1_NAME)")
#- instance 2 parameters
dbLoadRecords("sis8300bpm-instance.template","P=$(SYSTEM2_PREFIX),R=,PORT=$(PORT),ADDR=1,NAME=$(SYSTEM2_NAME)")

#- reference line 1 raw channel
iocshLoad("$(adsis8300bpm_DIR)/channel.iocsh","ADDR=0,PREFIX=$(PREFIX),CG_PREFIX=$(PREFIX),CH=RL1,NAME=Reference Line 1")

#- instance 1 raw channels
iocshLoad("$(adsis8300bpm_DIR)channel.iocsh","ADDR=1,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=A,NAME=Sensor A")
iocshLoad("$(adsis8300bpm_DIR)channel.iocsh","ADDR=2,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=B,NAME=Sensor B")
iocshLoad("$(adsis8300bpm_DIR)channel.iocsh","ADDR=3,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=C,NAME=Sensor C")
iocshLoad("$(adsis8300bpm_DIR)channel.iocsh","ADDR=4,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=D,NAME=Sensor D")
#- instance 2 raw channels
iocshLoad("$(adsis8300bpm_DIR)channel.iocsh","ADDR=5,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=A,NAME=Sensor A")
iocshLoad("$(adsis8300bpm_DIR)channel.iocsh","ADDR=6,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=B,NAME=Sensor B")
iocshLoad("$(adsis8300bpm_DIR)channel.iocsh","ADDR=7,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=C,NAME=Sensor C")
iocshLoad("$(adsis8300bpm_DIR)channel.iocsh","ADDR=8,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=D,NAME=Sensor D")

#- reference line 2 raw channel
iocshLoad("$(adsis8300bpm_DIR)channel.iocsh","ADDR=9,PREFIX=$(PREFIX),CG_PREFIX=$(PREFIX),CH=RL2,NAME=Reference Line 2")
#- instance 1 IQ channels
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=10,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=AM,NAME=A Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=11,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=BM,NAME=B Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=12,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=CM,NAME=C Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=13,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=DM,NAME=D Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=14,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=AA,NAME=A Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=15,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=BA,NAME=B Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=16,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=CA,NAME=C Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=17,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=DA,NAME=D Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=18,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=SM,NAME=SUM Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=19,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=SA,NAME=SUM Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=20,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=XP,NAME=X Position")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=21,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=YP,NAME=Y Position")
#- instance 2 IQ channels
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=22,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=AM,NAME=A Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=23,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=BM,NAME=B Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=24,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=CM,NAME=C Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=25,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=DM,NAME=D Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=26,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=AA,NAME=A Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=27,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=BA,NAME=B Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=28,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=CA,NAME=C Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=29,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=DA,NAME=D Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=30,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=SM,NAME=SUM Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=31,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=SA,NAME=SUM Angle")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=32,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=XP,NAME=X Position")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=33,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=YP,NAME=Y Position")
#- reference line 1 IQ channels
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=34,PREFIX=$(PREFIX),CG_PREFIX=$(PREFIX),CH=RL1-M,NAME=Reference Line 1 Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=35,PREFIX=$(PREFIX),CG_PREFIX=$(PREFIX),CH=RL1-A,NAME=Reference Line 1 Angle")
#- reference line 2 IQ channels
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=36,PREFIX=$(PREFIX),CG_PREFIX=$(PREFIX),CH=RL2-M,NAME=Reference Line 2 Magnitude")
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=37,PREFIX=$(PREFIX),CG_PREFIX=$(PREFIX),CH=RL2-A,NAME=Reference Line 2 Angle")
#- angle difference IQ channel
iocshLoad("$(adsis8300bpm_DIR)bpm_channel.iocsh","ADDR=38,PREFIX=$(PREFIX),CG_PREFIX=$(PREFIX),CH=DIFF-A,NAME=Angle Difference")
#- instance 1 averages
iocshLoad("$(adsis8300bpm_DIR)bpm_average.iocsh","ADDR=18,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=SM,NAME=SUM Magnitude average")
iocshLoad("$(adsis8300bpm_DIR)bpm_average.iocsh","ADDR=19,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=SA,NAME=SUM Angle average")
iocshLoad("$(adsis8300bpm_DIR)bpm_average.iocsh","ADDR=20,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=XP,NAME=X Position average")
iocshLoad("$(adsis8300bpm_DIR)bpm_average.iocsh","ADDR=21,PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX),CH=YP,NAME=Y Position average")
#- instance 2 averages
iocshLoad("$(adsis8300bpm_DIR)bpm_average.iocsh","ADDR=30,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=SM,NAME=SUM Magnitude average")
iocshLoad("$(adsis8300bpm_DIR)bpm_average.iocsh","ADDR=31,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=SA,NAME=SUM Angle average")
iocshLoad("$(adsis8300bpm_DIR)bpm_average.iocsh","ADDR=32,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=XP,NAME=X Position average")
iocshLoad("$(adsis8300bpm_DIR)bpm_average.iocsh","ADDR=33,PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX),CH=YP,NAME=Y Position average")
#- angle difference averages
iocshLoad("$(adsis8300bpm_DIR)bpm_average.iocsh","ADDR=38,PREFIX=$(PREFIX),CG_PREFIX=$(PREFIX),CH=DIFF-A,NAME=Angle Difference average")

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

# set_requestfile_path("$(E3_IOCSH_TOP)/autosave/req/")
# set_savefile_path("$(E3_IOCSH_TOP)/autosave/sav/")

#- apply default PV values (located in IOC_DIR)
epicsEnvSet("IOC_DIR", "$(E3_IOCSH_TOP)/..")
set_pass1_restoreFile("$(IOC_DIR)/default_settings_cg.sav", "P=$(PREFIX),R=")
set_pass1_restoreFile("$(IOC_DIR)/default_settings_system1.sav", "P=$(SYSTEM1_PREFIX),R=")
set_pass1_restoreFile("$(IOC_DIR)/default_settings_system2.sav", "P=$(SYSTEM2_PREFIX),R=")

#- apply runtime changed PV values (located in AUTOSAVE_DIR)
# set_pass1_restoreFile("$(E3_IOCSH_TOP)/autosave/sav/info_settings.sav")

save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("save_restoreStatus.db","P=$(PREFIX)")

###############################################################################
# BPM Manager Configurations
###############################################################################
dbLoadRecords("$(IOC_DIR)/bpmmanager_fanout.template", "P=$(SYSTEM1_PREFIX),R=,BPMSYSTEM=$(CONTROL_GROUP),G1=$(B1G1),G2=$(B1G2),G1POS=$(B1G1POS),G2POS=$(B1G2POS)")
dbLoadRecords("$(IOC_DIR)/bpmmanager_fanout.template", "P=$(SYSTEM2_PREFIX),R=,BPMSYSTEM=$(CONTROL_GROUP),G1=$(B2G1),G2=$(B2G2),G1POS=$(B2G1POS),G2POS=$(B2G2POS)")
iocMetadataConfigure "BPM" "$(PREFIX)"

###############################################################################
iocInit
###############################################################################

#- build info_settings.req file from record info 'autosaveFields' field
# makeAutosaveFiles("")
#- save things every thirty seconds
# create_monitor_set("$(E3_IOCSH_TOP)/autosave/req/info_settings.req", 30)

date
###############################################################################
