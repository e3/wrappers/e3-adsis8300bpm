###############################################################################

epicsEnvSet("CONTROL_GROUP",  "PBI-BPM01")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-140")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-6")
epicsEnvSet("EVR_NAME",       "PBI-BPM01:Ctrl-EVR-101:")

epicsEnvSet("SYSTEM1_PREFIX", "MEBT-010:PBI-BPM-007:")
epicsEnvSet("SYSTEM1_NAME",   "MEBT BPM 07")

epicsEnvSet("SYSTEM2_PREFIX", "MEBT-010:PBI-BPM-008:")
epicsEnvSet("SYSTEM2_NAME",   "MEBT BPM 08")

###############################################################################
