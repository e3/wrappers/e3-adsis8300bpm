###############################################################################

epicsEnvSet("CONTROL_GROUP",  "PBI-BPM01")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-120")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-4")
epicsEnvSet("EVR_NAME",       "PBI-BPM01:Ctrl-EVR-101:")

epicsEnvSet("SYSTEM1_PREFIX", "MEBT-010:PBI-BPM-003:")
epicsEnvSet("SYSTEM1_NAME",   "MEBT BPM 03")

epicsEnvSet("SYSTEM2_PREFIX", "MEBT-010:PBI-BPM-004:")
epicsEnvSet("SYSTEM2_NAME",   "MEBT BPM 04")

###############################################################################
