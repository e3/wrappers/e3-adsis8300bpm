###############################################################################

epicsEnvSet("CONTROL_GROUP",  "PBI-BPM01")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-130")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-5")
epicsEnvSet("EVR_NAME",       "PBI-BPM01:Ctrl-EVR-101:")

epicsEnvSet("SYSTEM1_PREFIX", "MEBT-010:PBI-BPM-005:")
epicsEnvSet("SYSTEM1_NAME",   "MEBT BPM 05")

epicsEnvSet("SYSTEM2_PREFIX", "MEBT-010:PBI-BPM-006:")
epicsEnvSet("SYSTEM2_NAME",   "MEBT BPM 06")

###############################################################################
