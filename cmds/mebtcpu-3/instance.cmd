###############################################################################

epicsEnvSet("CONTROL_GROUP",  "PBI-BPM01")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-110")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-3")
epicsEnvSet("EVR_NAME",       "PBI-BPM01:Ctrl-EVR-101:")

epicsEnvSet("SYSTEM1_PREFIX", "MEBT-010:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "MEBT BPM 01")

epicsEnvSet("SYSTEM2_PREFIX", "MEBT-010:PBI-BPM-002:")
epicsEnvSet("SYSTEM2_NAME",   "MEBT BPM 02")

###############################################################################
