###############################################################################

epicsEnvSet("CONTROL_GROUP",  "LAB-BPM20")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-006")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-6")
epicsEnvSet("EVR_NAME",       "LAB-BPM20:TS-EVR-000:")

epicsEnvSet("SYSTEM1_PREFIX", "LAB-020:PBI-BPM-003:")
epicsEnvSet("SYSTEM1_NAME",   "Lab BPM 20#01")

epicsEnvSet("SYSTEM2_PREFIX", "LAB-020:PBI-BPM-004:")
epicsEnvSet("SYSTEM2_NAME",   "Lab BPM 20#02")

# BPM1 - BPM Manager Options
epicsEnvSet("B1G1", "GRP1")
epicsEnvSet("B1G2", "Global")
epicsEnvSet("B1G1POS", "10")
epicsEnvSet("B1G2POS", "20")

# BPM1 - BPM Manager Options
epicsEnvSet("B2G1", "GRP2")
epicsEnvSet("B2G2", "Global")
epicsEnvSet("B2G1POS", "10")
epicsEnvSet("B2G2POS", "21")


###############################################################################
