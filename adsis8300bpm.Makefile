#
#  Copyright (c) 2019-present    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : joaopaulomartins
# email   : joaopaulomartins@esss.se
# Date    : 2019Dec10-1832-59CET
# version : 0.0.0 
#
# 

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



############################################################################
#
# Add any required modules here that come from startup scripts, etc.
#
############################################################################

############################################################################
#
# If you want to exclude any architectures:
#
############################################################################

EXCLUDE_ARCHS += linux-ppc64e6500


############################################################################
#
# Relevant directories to point to files
#
############################################################################

APP:=sis8300bpmApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src
DRV:=vendor/ess/lib


############################################################################
#
# Add any files that should be copied to $(module)/Db
#
############################################################################

USR_INCLUDES += -I$(where_am_I)$(DRV)
USR_INCLUDES += -I$(where_am_I)$(APPSRC)

TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(wildcard $(APPDB)/*.req)
TEMPLATES += $(wildcard ../template/*.sav)
TEMPLATES += $(wildcard ../template/*.template)
TEMPLATES += $(wildcard ../template/*.substitutions)

############################################################################
#
# Headers, sources, and DBD files
#
############################################################################

HEADERS += $(APPSRC)/sis8300bpm.h
SOURCES += $(APPSRC)/sis8300bpm.cpp
DBDS    += $(APPSRC)/sis8300bpmSupport.dbd

HEADERS += $(DRV)/sis8300drvbpm.h
SOURCES += $(DRV)/sis8300drvbpm.c


############################################################################
#
# Add any startup scripts that should be installed in the base directory
#
############################################################################

SCRIPTS += $(wildcard ../iocsh/*.iocsh)


.PHONY: 

#
# USR_DBFLAGS += -I . -I ..
# USR_DBFLAGS += -I $(EPICS_BASE)/db
# USR_DBFLAGS += -I $(APPDB)
#
# SUBS=$(wildcard $(APPDB)/*.substitutions)
# TMPS=$(wildcard $(APPDB)/*.template)
#
# # #	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
#	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
#	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db -S $@  > $(basename $(@)).db.d
#	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db -S $@

# #	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
#	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
#	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db $@  > $(basename $(@)).db.d
#	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db $@

#
# 
vlibs:

.PHONY: vlibs
